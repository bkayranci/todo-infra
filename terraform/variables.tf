variable "do_token" {
  type = string
}

variable "gitlab_access_token" {
  type = string
}

variable "k8s_version" {
  type    = string
  default = "1.21.2-do.0"
}

variable "k8s_region" {
  type    = string
  default = "nyc1"
}

variable "todo_ui_version" {
  type    = string
  default = "latest"
}

variable "todo_ui_helm_version" {
  type    = string
  default = "0.1.0"
}

variable "todo_api_version" {
  type    = string
  default = "latest"
}

variable "todo_api_helm_version" {
  type    = string
  default = "0.1.0"
}
