terraform {
  backend "http" {
  }
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "2.9.0"
    }
    helm = {
      source = "hashicorp/helm"
      version = "2.2.0"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.6.0"
    }
  }
}
