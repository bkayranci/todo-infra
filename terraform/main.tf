provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_kubernetes_cluster" "todo_k8s" {
  name    = "todo-k8s"
  region  = var.k8s_region
  version = var.k8s_version

  node_pool {
    name       = "autoscale-worker-pool"
    size       = "s-4vcpu-8gb"
    auto_scale = true
    min_nodes  = 1
    max_nodes  = 2
  }
}

provider "gitlab" {
  token = var.gitlab_access_token
}

resource "gitlab_project_variable" "todo_api_version" {
  project         = data.gitlab_project.todo_infra.id
  key             = "TF_VAR_todo_api_version"
  value           = var.todo_api_version
  protected       = true
}

resource "gitlab_project_variable" "todo_ui_version" {
  project         = data.gitlab_project.todo_infra.id
  key             = "TF_VAR_todo_ui_version"
  value           = var.todo_ui_version
  protected       = true
}

provider "helm" {
  kubernetes {
    host = digitalocean_kubernetes_cluster.todo_k8s.endpoint

    token = digitalocean_kubernetes_cluster.todo_k8s.kube_config[0].token
    cluster_ca_certificate = base64decode(
      digitalocean_kubernetes_cluster.todo_k8s.kube_config[0].cluster_ca_certificate
    )

  }
}


resource "digitalocean_database_cluster" "todo_pg" {
  name       = "todo-postgres"
  engine     = "pg"
  version    = "13"
  size       = "db-s-1vcpu-1gb"
  region     = "nyc1"
  node_count = 1
}

resource "helm_release" "ingress" {
  repository = "https://helm.nginx.com/stable"
  name    = "nginx-ingress"
  chart   = "nginx-ingress"
  version = "0.10.0"
}

resource "helm_release" "todo_ui_helm" {
  name        = "todo-ui"
  chart       = "https://gitlab.com/api/v4/projects/27814296/packages/generic/todo-ui-helm/${var.todo_ui_helm_version}/todo-ui-${var.todo_ui_helm_version}.tgz"
  version     = var.todo_ui_helm_version

  set {
    name = "image.tag"
    value = var.todo_ui_version
  }
}

resource "helm_release" "todo_api_helm" {
  name        = "todo-api"
  chart       = "https://gitlab.com/api/v4/projects/27809947/packages/generic/todo-api-helm/${var.todo_api_helm_version}/todo-api-${var.todo_api_helm_version}.tgz"
  version     = var.todo_api_helm_version

  set {
    name = "image.tag"
    value = var.todo_api_version
  }

  set {
    name  = "env.dbURI"
    value = digitalocean_database_cluster.todo_pg.uri
  }
}
